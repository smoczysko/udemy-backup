package Manipulation;

import org.hamcrest.CoreMatchers;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by robert.kmieciak on 09.12.13.
 */
public class FirstManipulation {
    static WebDriver driver;
    final static String testPageURL =
            "http://www.compendiumdev.co.uk" +
                    "/selenium/basic_html_form.html";
    String pageTitle = "HTML Form Elements";

    @BeforeClass
    public static void startDriver() {
        driver = new FirefoxDriver();
        driver.navigate().to(testPageURL);
    }

    @AfterClass
    public static void closeDriver() {
        driver.quit();
    }

    @Test
    public void myFirstManipulation() {
        driver.get(testPageURL);

        submit();

        Assert.assertNotSame(driver.getTitle(), pageTitle);

        driver.navigate().back();
    }

    @Test
    public void textInputTest(){
        //text input
        WebElement textInputForm = driver.findElement(By.
                xpath(".//*[@id='HTMLFormElements']/table/tbody/tr[3]/td/textarea"));
        textInputForm.clear();
        textInputForm.sendKeys("comments");
        submit();

        Assert.assertNotSame(driver.findElement(By.
                xpath(".//*[@id='_valuecomments']")).getText(),
                "comments");

        driver.navigate().back();
    }

    @Test
    public void radioButton(){
        //radio
        WebElement radio2 = driver.findElement(By.
                xpath("/html/body/form/table/tbody/tr[6]/td/input[2]"));
        if(!radio2.isSelected()){
            radio2.click();
        }

        submit();

        WebElement radioResult;
        radioResult = driver.findElement(By.id("_valueradioval"));

    }

    @Test
    public void checkBoxes(){
        //checkbox
        WebElement checkBox1 = driver.findElement(By.
                xpath("/html/body/form/table/tbody/tr[5]/td/input"));
        if(!checkBox1.isSelected()){
            checkBox1.click();
        }

        WebElement checkBox2 = driver.findElement(By.
                xpath("/html/body/form/table/tbody/tr[5]/td/input[2]"));
        if(checkBox2.isSelected()){
            checkBox2.click();
        }

        WebElement checkBox3 = driver.findElement(By.
                xpath("/html/body/form/table/tbody/tr[5]/td/input[2]"));
        if(checkBox3.isSelected()){
            checkBox3.click();
        }

        submit();

        waitForOption();

        WebElement checkBoxResult;
        checkBoxResult = driver.findElement(By.xpath("//li[@id='_valuecheckboxes0']"));

        Assert.assertTrue(checkBoxResult != null);
    }

    @Test
    public void dropDown() {
        //dropdown
        WebElement dropDown5 = driver.findElement(By.
                xpath("/html/body/form/table/tbody/tr[8]/td/select/option[5]"));
        if(!dropDown5.isSelected()){
            dropDown5.click();
        }

        submit();

        waitForOption();

        WebElement dropDownResult;
        dropDownResult = driver.findElement(By.id("_valuedropdown"));

        Assert.assertEquals(dropDownResult.getText(), "dd5");
    }

    @Test
    public void multiSelect() {
        //multi select
        WebElement multiSelect;

        multiSelect = driver.findElement(By.
                xpath("/html/body/form/table/tbody/tr[7]/td/select"));
        List<WebElement> multiSelectOptions = multiSelect.findElements(By.tagName("option"));

        multiSelectOptions.get(0).click();
        multiSelectOptions.get(1).click();
        multiSelectOptions.get(2).click();

        if(multiSelectOptions.get(3).isSelected()){
            multiSelectOptions.get(3).click();
        }

        submit();
        waitForOption();

        Assert.assertEquals("ms1", driver.findElement(By.id("_valuemultipleselect0")).getText());
        Assert.assertEquals("ms2", driver.findElement(By.id("_valuemultipleselect1")).getText());
        Assert.assertEquals("ms3", driver.findElement(By.id("_valuemultipleselect2")).getText());
    }

    private void submit(){
        WebElement element = driver.findElement(By.
                xpath(".//*[@id='HTMLFormElements']/table/tbody/tr[9]/td/input[2]"));
        element.click();
    }


    private void waitForOption() {
        new WebDriverWait(driver, 300).until(ExpectedConditions.titleIs("Processed Form Details"));
    }
}
