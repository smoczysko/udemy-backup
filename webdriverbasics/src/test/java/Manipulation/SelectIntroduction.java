package Manipulation;

import junit.framework.Assert;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by robert.kmieciak on 10.12.13.
 */
public class SelectIntroduction {
    static WebDriver driver;
    final static String testPageURL =
            "http://www.compendiumdev.co.uk" +
                    "/selenium/basic_html_form.html";
    String pageTitle = "HTML Form Elements";

    @BeforeClass
    public static void startDriver() {
        driver = new FirefoxDriver();
        driver.navigate().to(testPageURL);
    }

    @AfterClass
    public static void closeDriver() {
        driver.quit();
    }

    @Test
    public void deselectAll(){
        WebElement multi;
        multi = driver.findElement(By.xpath("/html/body/form/table/tbody/tr[7]/td/select"));
        Select multipleSelect = new Select(multi);

        multipleSelect.deselectAll();
        Assert.assertTrue(!multi.isSelected());
    }

    @Test
    public void deselectChosen(){
        WebElement multi;
        multi = driver.findElement(By.xpath("/html/body/form/table/tbody/tr[7]/td/select"));
        Select multipleSelect = new Select(multi);

        multipleSelect.deselectByIndex(3);
        multipleSelect.selectByIndex(1);

        submit();

        Assert.assertEquals("ms2", driver.findElement(By.id("_valuemultipleselect0")).getText());
//        Assert.assertFalse(driver.findElement(By.id("_valuemultipleselect3")).isDisplayed());
    }

    private void submit(){
        WebElement element = driver.findElement(By.
                xpath(".//*[@id='HTMLFormElements']/table/tbody/tr[9]/td/input[2]"));
        element.click();
    }

    private void waitForOption() {
        new WebDriverWait(driver, 300).until(ExpectedConditions.titleIs("Processed Form Details"));
    }
}
