package Manipulation;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.hamcrest.core.Is.is;

/**
 * Created by robert.kmieciak on 08.12.13.
 */
public class WebDriverWaitIntroduction {
    static WebDriver driver;
    final static String testPageURL =
            "http://www.compendiumdev.co.uk" +
                    "/selenium/basic_html_form.html";
    String pageTitle = "Welcome to the Find By Playground";

    @BeforeClass
    public static void startDriver() {
        driver = new FirefoxDriver();
        driver.navigate().to(testPageURL);
    }

    @AfterClass
    public static void closeDriver() {
        driver.quit();
    }

    @Test
         public void myFirstManipulation() {
        driver.get(testPageURL);

        new WebDriverWait(driver, 10).until(ExpectedConditions.titleIs("HTML Form Elements"));
    }

    @Test
    public void mySecondManipulation() {
        driver.get(testPageURL);

        new WebDriverWait(driver, 10, 50).until(ExpectedConditions.titleIs("HTML Form Elements"));
    }
}
