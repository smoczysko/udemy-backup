package UserInteractions;

import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

/**
 * Created by robert.kmieciak on 11.12.13.
 */
public class FirstExerciesTest {
    static WebDriver driver;
    final static String testPageURL =
            "http://www.compendiumdev.co.uk" +
                    "/selenium/gui_user_interactions.html";
    String pageTitle = "GUI User Interactions";

    @BeforeClass
    public static void startDriver() {
        driver = new FirefoxDriver();
        driver.navigate().to(testPageURL);
    }

    @AfterClass
    public static void closeDriver() {
        driver.quit();
    }

    @Test
    public void drag1overDrop1() {
        WebElement draggable1 = driver.findElement(By.cssSelector("div[id='draggable1']"));
        WebElement droppable1 = driver.findElement(By.cssSelector("div[id='droppable1']"));

        Actions drag1 = new Actions(driver);

        drag1.clickAndHold(draggable1).moveToElement(droppable1).release().perform();

        Assert.assertEquals("Dropped!", droppable1.getText());
    }

    @Test
    public void drag2overDrop1() {
        WebElement draggable2 = driver.findElement(By.cssSelector("div[id='draggable2']"));
        WebElement droppable1 = driver.findElement(By.cssSelector("div[id='droppable1']"));

        Actions drag1 = new Actions(driver);

        drag1.clickAndHold(draggable2).moveToElement(droppable1).release().perform();

        Assert.assertEquals("Get Off Me!", droppable1.getText());
    }

    @Test
    public void controlBTest() {
//////////////////////////////////////////////////////////////////////////////
//
//                            Mój kod
//////////////////////////////////////////////////////////////////////////////
//
       WebElement draggable1 = driver.findElement(By.id("draggable1"));

        driver.findElement(By.tagName("html")).click();

        new Actions(driver).keyDown(Keys.CONTROL).sendKeys("b").keyUp(Keys.CONTROL).perform();

        Assert.assertEquals("Bwa! Ha! Ha!", draggable1.getText());

//        WebElement draggable1 = driver.findElement(By.id("draggable1"));
//
//        Actions actions = new Actions(driver);
//
//        draggable1.click();
//
//        new Actions(driver).keyDown(Keys.CONTROL).
//                sendKeys("b").
//                keyUp(Keys.CONTROL).
//                perform();
//
//        assertEquals("Bwa! Ha! Ha!", draggable1.getText());
    }

    @Ignore
    @Test
    public void letGoTest() {
        WebElement droppable1 = driver.findElement(By.cssSelector("div[id='droppable1']"));
        WebElement droppable2 = driver.findElement(By.cssSelector("div[id='droppable2']"));

        new Actions(driver).keyDown(Keys.CONTROL).keyDown(Keys.SPACE).keyUp(Keys.CONTROL).keyUp(Keys.SPACE).perform();

        Assert.assertEquals("Let GO!!", droppable1.getText());
        Assert.assertEquals("Let GO!!", droppable2.getText());
    }

    @Test
    public void draw() {
        WebElement canvas = driver.findElement(By.id("canvas"));
        WebElement events = driver.findElement(By.id("keyeventslist"));

        int eventCount = events.findElements(By.tagName("li")).size();

        Actions draw = new Actions(driver);

        draw.clickAndHold(canvas).moveByOffset(15, 5).release().perform();

        Assert.assertTrue(events.findElements(By.tagName("li")).size() > eventCount);
    }
}
