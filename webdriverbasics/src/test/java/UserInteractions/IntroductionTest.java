package UserInteractions;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

/**
 * Created by robert.kmieciak on 11.12.13.
 */
public class IntroductionTest {

    static WebDriver driver;
    final static String testPageURL =
            "http://www.compendiumdev.co.uk" +
                    "/selenium/basic_html_form.html";
    String pageTitle = "HTML Form Elements";

    @BeforeClass
    public static void startDriver() {
        driver = new FirefoxDriver();
        driver.navigate().to(testPageURL);
    }

    @AfterClass
    public static void closeDriver() {
        driver.quit();
    }

    @Test
    public void introduction() {
        Action clickOnCb1 = new Actions(driver).click(driver.findElement(By.cssSelector("input[value='cb1']"))).build();

        clickOnCb1.perform();
        clickOnCb1.perform();
        clickOnCb1.perform();

        System.out.println("BreakPoint");
    }
}
