package FramesAndWindows;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static junit.framework.Assert.assertEquals;

/**
 * Created by robert.kmieciak on 16.12.13.
 */
public class ManagingWindows {
    private WebDriver driver;
    private WebDriverWait wait;
    final static String testPageURL =
            "http://www.compendiumdev.co.uk" +
                    "/selenium/bounce.html";

    @Before
    public  void startDriver() {
        driver = new FirefoxDriver();
        driver.navigate().to(testPageURL);
        wait = new WebDriverWait(driver, 10);
    }

    @After
    public  void closeDriver() {
        driver.quit();
    }

    @Test
    public void manageWindowFirst(){
        driver.manage().window().setPosition(new Point(10, 20));

        Point pos = driver.manage().window().getPosition();

        assertEquals(10, pos.getX());
        assertEquals(20, pos.getY());

        driver.manage().window().setSize(new Dimension(350, 400));
        Dimension winSizes = driver.manage().window().getSize();

        assertEquals(350, winSizes.getWidth());
        assertEquals(400, winSizes.getHeight());
    }

    @Test
    public void manageWindowSecond(){
        driver.manage().window().maximize();

        Dimension winSizes = driver.manage().window().getSize();

        driver.manage().window().setSize(new Dimension(winSizes.getWidth()/2, winSizes.getHeight()/2));

        Point pos = driver.manage().window().getPosition();

        driver.manage().window().setPosition(new Point(pos.getX()/2, pos.getY()/2));
    }

    //TODO: Bouncing
}
