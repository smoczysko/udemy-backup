package FramesAndWindows;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.sql.Driver;

/**
 * Created by robert.kmieciak on 16.12.13.
 */
public class FramesAndWindowsIntroduction {
    static WebDriver driver;
    final static String testPageURL =
            "http://www.compendiumdev.co.uk" +
                    "/selenium/frames";

    @BeforeClass
    public static void startDriver() {
        driver = new FirefoxDriver();
        driver.navigate().to(testPageURL);
    }

    @AfterClass
    public static void closeDriver() {
        driver.quit();
    }

    @Test
    public void greenPage() {
        driver.switchTo().frame("content");
        driver.findElement(By.cssSelector("a[href='green.html']")).click();

        Assert.assertEquals("Green Page", driver.findElement(By.tagName("h1")).getText());

        driver.findElement(By.cssSelector("a[href='content.html']")).click();
        Assert.assertEquals("Content", driver.findElement(By.tagName("h1")).getText());
    }

    @Test
    public void example5() {
        driver.switchTo().frame("menu");
        driver.findElement(By.cssSelector("a[href='iframe.html']")).click();

        new WebDriverWait(driver, 10).
                until(ExpectedConditions.titleIs("HTML Frames Example - iFrame Contents"));

        driver.switchTo().frame(0);
        driver.findElement(By.cssSelector("a[href='frames_example_5.html']")).click();

        new WebDriverWait(driver, 10).
                until(ExpectedConditions.titleIs("Frameset Example Title (Example 5)"));

        driver.switchTo().frame("content");
        driver.findElement(By.cssSelector("a[href='index.html']")).click();

        new WebDriverWait(driver, 10).
                until(ExpectedConditions.titleIs("Frameset Example Title (Example 6)"));

        //zrobilem frame a nie frameset i przez to mi nie przechodzily testy xD
    }
}
