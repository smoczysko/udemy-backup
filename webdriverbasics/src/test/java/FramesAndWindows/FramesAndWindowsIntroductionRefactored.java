package FramesAndWindows;

import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static junit.framework.Assert.assertEquals;
import static org.openqa.selenium.support.ui.ExpectedConditions.titleIs;

/**
 * Created by robert.kmieciak on 16.12.13.
 */
public class FramesAndWindowsIntroductionRefactored {
    private WebDriver driver;
    private WebDriverWait wait;
    final static String testPageURL =
            "http://www.compendiumdev.co.uk" +
                    "/selenium/frames";

    @Before
    public  void startDriver() {
        driver = new FirefoxDriver();
        driver.navigate().to(testPageURL);
        wait = new WebDriverWait(driver, 10);
    }

    @After
    public  void closeDriver() {
        driver.quit();
    }

    @Test
    public void greenPage() {
        driver.switchTo().frame("content");
        driver.findElement(By.cssSelector("a[href='green.html']")).click();

        assertEquals("Green Page", driver.findElement(By.tagName("h1")).getText());

        driver.findElement(By.cssSelector("a[href='content.html']")).click();
        assertEquals("Content", driver.findElement(By.tagName("h1")).getText());
    }

    @Test
    public void example5() {
        driver.switchTo().frame("menu");
        driver.findElement(By.cssSelector("a[href='iframe.html']")).click();

        wait.until(titleIs("HTML Frames Example - iFrame Contents"));

        driver.switchTo().frame(0);
        driver.findElement(By.cssSelector("a[href='frames_example_5.html']")).click();

        wait.until(titleIs("Frameset Example Title (Example 5)"));

        driver.switchTo().frame("content");
        driver.findElement(By.cssSelector("a[href='index.html']")).click();

        wait.until(titleIs("Frameset Example Title (Example 6)"));

        //zrobilem frame a nie frameset i przez to mi nie przechodzily testy xD
    }
}
