package PageObjectsRefactored;

import PageObjectsRefactored.pages.BasicAjaxPageObject;
import PageObjectsRefactored.pages.ProcessedFormPage;
import com.seleniumsimplified.webdriver.manager.Driver;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static PageObjectsRefactored.pages.BasicAjaxPageObject.Category;
import static PageObjectsRefactored.pages.BasicAjaxPageObject.Language;

public class BasicTestRefactored {

    private WebDriver driver;
    private BasicAjaxPageObject basicAjaxPage;

    @Before
    public void setup(){
        driver = Driver.get();

        basicAjaxPage = new BasicAjaxPageObject(driver);
        basicAjaxPage.get();
    }

    @Test
    // Exercise: Feel The Pain
    public void chooseToCodeInJavaOnTheServerFromCombosNoSynchronisationExample(){

        try{
            WebDriver driver;

            driver = Driver.get("http://compendiumdev.co.uk/selenium/" +
                    "basic_ajax.html");

            // select Server
            WebElement categorySelect = driver.findElement(By.id("combo1"));
            categorySelect.findElement(By.cssSelector("option[value='3']")).click();

            // then select Java
            WebElement languageSelect = driver.findElement(By.id("combo2"));
            languageSelect.findElement(By.cssSelector("option[value='23']")).click();

            // Submit the form
            WebElement codeInIt = driver.findElement(By.name("submitbutton"));
            codeInIt.click();

            WebElement languageWeUsed = driver.findElement(By.id("_valuelanguage_id"));
            assertEquals("Expected Java code", "23",languageWeUsed.getText());

        }catch(Exception e){
            assertTrue("Expected some sort of exception thrown",true);
        }
    }

    @Test
    public void chooseToCodeInJavaOnTheServerFromCombosSyncOnAjaxBusyExample(){


        basicAjaxPage.selectCategory(Category.SERVER);

        new WebDriverWait(driver,10).until(
                basicAjaxPage.ajaxActionIsComplete());

        basicAjaxPage.selectLanguage(Language.JAVA);
        basicAjaxPage.clickCodeInIt();

        ProcessedFormPage processedFormPage = new ProcessedFormPage(driver);
        processedFormPage.waitUntilPageIsLoaded();

        assertEquals("Expected Java code", "23",processedFormPage.getValueFor("language_id"));
    }

    @Test
    public void chooseToCodeInJavascriptOnTheWeb(){
        //to avoid wrong value for JAVASCRIPT I need to switch category
        basicAjaxPage.selectCategory(Category.SERVER);

        basicAjaxPage.selectCategory(Category.WEB);

        new WebDriverWait(driver,10).until(
                basicAjaxPage.ajaxActionIsComplete());

        basicAjaxPage.selectLanguage(Language.JAVASCRIPT);
        basicAjaxPage.clickCodeInIt();

        ProcessedFormPage processedFormPage = new ProcessedFormPage(driver);
        processedFormPage.waitUntilPageIsLoaded();

        assertEquals("Expected Javascript code", "0",processedFormPage.getValueFor("language_id"));
    }

    @Test
    public void chooseToCodeInCppOnDesktop(){

        basicAjaxPage.selectCategory(Category.DESKTOP);

        // then select Java

        basicAjaxPage.selectLanguage(Language.CPP_DESKTOP);
        basicAjaxPage.clickCodeInIt();

        ProcessedFormPage processedFormPage = new ProcessedFormPage(driver);
        processedFormPage.waitUntilPageIsLoaded();

        assertEquals("Expected C++ code", "10",processedFormPage.getValueFor("language_id"));

    }
}