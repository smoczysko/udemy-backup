package PageObjectsRefactored.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasicAjaxPageObject {

    private WebDriver driver;

    public enum Category {WEB(1), DESKTOP(2), SERVER(3);
        private int dropDownValue;

        Category(int value){
            this.dropDownValue = value;
        }

        public int value() {
            return dropDownValue;
        }
    }

    public enum Language {JAVASCRIPT(0), VBSCRIPT(1), FLASH(2),
                        CPP_DESKTOP(10), AsSEMBLER(11), C(12), VISUAL_BASIC(13),
                        COBOL(20), FORTRAN(21), CPP_SERVER(22), JAVA(23);
        private int dropDownValue;

        Language(int value){
            this.dropDownValue = value;
        }

        public int value() {
            return dropDownValue;
        }
    }

    public BasicAjaxPageObject(WebDriver aDriver) {
        driver = aDriver;
    }

    public void get() {
        driver.get("http://compendiumdev.co.uk/selenium/basic_ajax.html");
    }

    public void selectCategory(Category category) {
        WebElement categorySelect = driver.findElement(By.id("combo1"));
        categorySelect.findElement(By.cssSelector("option[value='" + category.value() + "']")).click();

        /* wait until the option I want to click is present
        new WebDriverWait(driver,10).until(
                ExpectedConditions.presenceOfElementLocated(
                        By.cssSelector("option[value='23']")));*/

        new WebDriverWait(driver,100).until(ajaxActionIsComplete());
    }

    public ExpectedCondition<Boolean> ajaxActionIsComplete() {
        return ExpectedConditions.invisibilityOfElementLocated(
                By.id("ajaxBusy"));
    }

    public void selectLanguage(Language language) {
        WebElement languageSelect = driver.findElement(By.id("combo2"));
        languageSelect.findElement(By.cssSelector("option[value='" + language.value() + "']")).click();
    }

    public void clickCodeInIt() {
        WebElement codeInIt = driver.findElement(By.name("submitbutton"));
        codeInIt.click();
    }
}
