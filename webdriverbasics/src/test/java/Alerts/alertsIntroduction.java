package Alerts;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by robert.kmieciak on 16.12.13.
 */
public class alertsIntroduction {
    static WebDriver driver;
    final static String testPageURL =
            "http://www.compendiumdev.co.uk" +
                    "/selenium/alerts.html";

    @BeforeClass
    public static void startDriver() {
        driver = new FirefoxDriver();
        driver.navigate().to(testPageURL);
    }

    @AfterClass
    public static void closeDriver() {
        driver.quit();
    }

    @Test
    public void basicAlert() {
        driver.navigate().refresh();

        WebElement alertButton;

        alertButton = driver.findElement(By.id("alertexamples"));
        alertButton.click();

        String allertMessage = "I am an alert box!";
        Assert.assertEquals(allertMessage, driver.switchTo().alert().getText());

        driver.switchTo().alert().accept();
    }

    @Test
    public void basicAlertDismiss() {
        driver.navigate().refresh();

        WebElement alertButton;

        alertButton = driver.findElement(By.id("alertexamples"));
        alertButton.click();

        String allertMessage = "I am an alert box!";
        Assert.assertEquals(allertMessage, driver.switchTo().alert().getText());

        driver.switchTo().alert().dismiss();
    }

    @Test
    public void confirmBoxAccept() {
        driver.navigate().refresh();

        WebElement confirmButton;

        String beforeClick = driver.findElement(By.id("confirmreturn")).getText();
        Assert.assertEquals("cret", beforeClick);

        confirmButton = driver.findElement(By.id("confirmexample"));
        confirmButton.click();

        String confirmMessage = "I am a confirm alert";
        Assert.assertEquals(confirmMessage, driver.switchTo().alert().getText());

        driver.switchTo().alert().accept();

        String returned = driver.findElement(By.id("confirmreturn")).getText();

        Assert.assertEquals("true", returned);
    }

    @Test
    public void confirmBoxDismiss() {
        driver.navigate().refresh();

        WebElement confirmButton;

        String beforeClick = driver.findElement(By.id("confirmreturn")).getText();
        Assert.assertEquals("cret", beforeClick);

        confirmButton = driver.findElement(By.id("confirmexample"));
        confirmButton.click();

        String confirmMessage = "I am a confirm alert";
        Assert.assertEquals(confirmMessage, driver.switchTo().alert().getText());

        driver.switchTo().alert().dismiss();

        String returned = driver.findElement(By.id("confirmreturn")).getText();

        Assert.assertEquals("false", returned);
    }

    @Test
    public void promptBoxAccept() {
        driver.navigate().refresh();

        WebElement showPrompt;

        String beforeClick = driver.findElement(By.id("promptreturn")).getText();
        Assert.assertEquals("pret", beforeClick);

        showPrompt = driver.findElement(By.id("promptexample"));
        showPrompt.click();

        String confirmMessage = "I prompt you";
        Assert.assertEquals(confirmMessage, driver.switchTo().alert().getText());

        driver.switchTo().alert().accept();

        String returned = driver.findElement(By.id("promptreturn")).getText();

        Assert.assertEquals("change me", returned);
    }

    @Test
    public void promptBoxDismiss() {
        driver.navigate().refresh();

        WebElement showPrompt;

        String beforeClick = driver.findElement(By.id("promptreturn")).getText();
        Assert.assertEquals("pret", beforeClick);

        showPrompt = driver.findElement(By.id("promptexample"));
        showPrompt.click();

        String confirmMessage = "I prompt you";
        Assert.assertEquals(confirmMessage, driver.switchTo().alert().getText());

        driver.switchTo().alert().dismiss();

        String returned = driver.findElement(By.id("promptreturn")).getText();

        Assert.assertEquals("pret", returned);
    }

    @Test
    public void changeText() {
        driver.navigate().refresh();

        WebElement showPrompt;

        String beforeClick = driver.findElement(By.id("promptreturn")).getText();
        Assert.assertEquals("pret", beforeClick);

        showPrompt = driver.findElement(By.id("promptexample"));
        showPrompt.click();

        String confirmMessage = "I prompt you";

        Alert promptAlert = driver.switchTo().alert();

        Assert.assertEquals(confirmMessage, promptAlert.getText());

        promptAlert.sendKeys("I am new prompt");
        promptAlert.accept();

        String returned = driver.findElement(By.id("promptreturn")).getText();

        Assert.assertEquals("I am new prompt", returned);
    }
}
