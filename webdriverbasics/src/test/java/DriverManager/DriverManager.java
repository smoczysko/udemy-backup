package DriverManager;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public class DriverManager {

    public static final String SELENIUM2_BASICS_DRIVER = "selenium2Basics.driver";

    public static WebDriver get() {

        String browserToUse = "";
        if(System.getProperties().containsKey(SELENIUM2_BASICS_DRIVER)){
            browserToUse = System.getProperty(SELENIUM2_BASICS_DRIVER);
        }

        if (browserToUse.equals("firefox")) {
            return new FirefoxDriver();
        } else if (browserToUse.equals("chrome")) {
            return new ChromeDriver();
        } else {
            return new HtmlUnitDriver();
        }
    }
}
