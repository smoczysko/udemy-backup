package DriverManager;


import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import static org.hamcrest.core.Is.is;

public class DriverManagerExercise {

    WebDriver driver;

    @Test
    public void createAFirefoxDriver() {
        System.setProperty(DriverManager.SELENIUM2_BASICS_DRIVER,"firefox");

        assertBrowserTestRuns();
    }

    @Test
    public void createAnChromeDriver() {
        System.setProperty(DriverManager.SELENIUM2_BASICS_DRIVER,"chrome");

        assertBrowserTestRuns();
    }

    @Test
    public void createADefaultDriver() {
        assertBrowserTestRuns();
    }

    public void assertBrowserTestRuns() {
        driver = DriverManager.get();

        driver.get("http://compendiumdev.co.uk/selenium/basic_web_page.html");

        Assert.assertThat(driver.getTitle(), is("Basic Web Page Title"));
    }
    @After
    public void quitDriver() {
        driver.quit();
    }
}
