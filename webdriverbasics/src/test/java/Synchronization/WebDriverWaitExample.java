package Synchronization;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.openqa.selenium.support.ui.ExpectedConditions.titleIs;


/**
 * Created by robert.kmieciak on 03.01.14.
 */
public class WebDriverWaitExample {
    private WebDriver driver;
    private WebDriverWait wait;
    final static String testPageURL =
            "http://www.compendiumdev.co.uk" +
                    "/selenium/basic_html_form.html";

    @Before
    public  void startDriver() {
        driver = new FirefoxDriver();
        driver.navigate().to(testPageURL);
        wait = new WebDriverWait(driver, 100);
    }

    @After
    public  void closeDriver() {
        driver.quit();
    }

    @Test
    public void exampleUsingExpectedConditions(){
        wait.until(titleIs("HTML Form Elements"));

        // asserted in line above Assert.assertEquals("HTML Form Elements", driver.getTitle());
    }

    @Test
    public void exampleWithSleep() {
        wait.until(titleIs("HTML Form Elements"));

        // asserted in line above Assert.assertEquals("HTML Form Elements", driver.getTitle());
    }
}
