package Synchronization;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.hamcrest.core.Is.is;

/**
 * Created by robert.kmieciak on 17.12.13.
 */
public class SynchronizationExample {
    private WebDriver driver;
    private WebDriverWait wait;
    final static String testPageURL =
            "http://www.compendiumdev.co.uk" +
                    "/selenium/basic_ajax.html";

    @Before
    public  void startDriver() {
        driver = new FirefoxDriver();
        driver.navigate().to(testPageURL);
        wait = new WebDriverWait(driver, 100);
    }

    @After
    public  void closeDriver() {
        driver.quit();
    }

    @Test
    public void exampleTest() throws InterruptedException {
        WebElement combo1 = driver.findElement(By.id("combo1"));
        combo1.findElement(By.cssSelector("option[value='3']")).click();

//        new WebDriverWait(driver,10).until(
//                ExpectedConditions.presenceOfElementLocated(
//                        By.cssSelector("option[value='3']")));


        // znikajacy loader
//        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("ajaxBusy")));

        // waiting until it is possile to select option 23
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("option[value='23']")));

        WebElement combo2 = driver.findElement(By.id("combo2"));
        combo2.findElement(By.cssSelector("option[value='23']")).click();

//        new WebDriverWait(driver,10).until(
//                ExpectedConditions.presenceOfElementLocated(
//                        By.cssSelector("option[value='23']")));

        WebElement button = driver.findElement(By.name("submitbutton"));
        button.click();

        Assert.assertThat(driver.findElement(By.id("_valuelanguage_id")).getText(), is("23"));
    }
}
