package Synchronization;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.hamcrest.core.Is.is;

/**
 * Created by robert.kmieciak on 03.01.14.
 */
public class CustomExpectedConditions {
    private WebDriver driver;
    private WebDriverWait wait;
    final static String testPageURL =
            "http://www.compendiumdev.co.uk" +
                    "/selenium/basic_ajax.html";

    @Before
    public  void startDriver() {
        driver = new FirefoxDriver();
        driver.navigate().to(testPageURL);
        wait = new WebDriverWait(driver, 100);
    }

    @After
    public  void closeDriver() {
        driver.quit();
    }

    @Test
    public void useCustomExpectedCondition() {
        WebElement combo1 = driver.findElement(By.id("combo1"));
        combo1.findElement(By.cssSelector("option[value='3']")).click();

        new WebDriverWait(driver, 5000).until(
                new SelectContainsText(By.id("combo2"),"Java"));

        WebElement combo2 = driver.findElement(By.id("combo2"));
        combo2.findElement(By.cssSelector("option[value='23']")).click();

        WebElement button = driver.findElement(By.name("submitbutton"));
        button.click();

        Assert.assertThat(driver.findElement(By.id("_valuelanguage_id")).getText(), is("23"));
    }

    private class SelectContainsText implements ExpectedCondition<Boolean> {
        private String textToFind;
        private By findBy;

        public SelectContainsText(final By comboFindBy, final String textToFind) {
            this.findBy = comboFindBy;
            this.textToFind = textToFind;
        }

        @Override
        public Boolean apply(WebDriver webDriver) {
            WebElement combo = webDriver.findElement(this.findBy);
            List<WebElement> options = combo.findElements(By.tagName("option"));

            for(WebElement anOption : options) {
                if(anOption.getText().equals(this.textToFind))
                    return true;
            }

            return false;
        }

        @Override
        public String toString() {
            return "select " + this.findBy + " to contain " + this.textToFind;
        }
    }
}