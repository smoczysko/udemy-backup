package Synchronization;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.junit.Assert;

/**
 * Created by robert.kmieciak on 03.01.14.
 */
public class ExercisesTest {
    private WebDriver driver;
    private WebDriverWait wait;
    final static String testPageURL =
            "http://www.compendiumdev.co.uk" +
                    "/selenium/basic_ajax.html";

    @Before
    public  void startDriver() {
        driver = new FirefoxDriver();
        driver.navigate().to(testPageURL);
        wait = new WebDriverWait(driver, 100);
    }

    @After
    public  void closeDriver() {
        driver.quit();
    }

    @Test
    public void canReturnAWebElementInsteadOfABooleanUsingAnonymousClass() {
        driver.get(testPageURL);

        WebElement categorySelect = driver.findElement(By.id("combo1"));
        categorySelect.findElement(By.cssSelector("option[value='3']")).click();

        WebElement elly = new WebDriverWait(driver,10).until(
                new ExpectedCondition<WebElement>() {
                    @Override
                    public WebElement apply(WebDriver webDriver) {

                        WebElement eli = webDriver.findElement(
                                By.cssSelector("option[value='23']"));

                        if(eli.isDisplayed()){
                            return eli;
                        }else{
                            return null;
                        }
                    }
                }
        );

        elly.click();
        Assert.assertEquals("Expected Java", "Java", elly.getText());
    }

    @Test
    public void customExpectedConditionForTitleDoesNotContainUsingClass(){
        driver.get("http://www.compendiumdev.co.uk/selenium/basic_redirect.html");

        driver.findElement((By.id("delaygotobasic"))).click();

        Assert.assertEquals("Basic Web Page Title", new WebDriverWait(driver,8).until(
                new TitleDoesNotContain("Redirects")));
    }

    private class TitleDoesNotContain implements ExpectedCondition<String>{
        private String titleExcludes;

        public TitleDoesNotContain(String notContainThisString) {
            this.titleExcludes = notContainThisString;
        }

        @Override
        public String apply(WebDriver webDriver) {
            String title = webDriver.getTitle();

            if(title.contains(this.titleExcludes)){
                return null;
            } else {
                return title;
            }
        }
    }
}
