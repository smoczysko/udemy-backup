package Synchronization;

import com.google.common.base.Function;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;


import java.util.concurrent.TimeUnit;

/**
 * Created by robert.kmieciak on 03.01.14.
 */
public class WebDriverWaitFluently {
    private WebDriver driver;
    private WebDriverWait wait;
    WebElement countdown;
    final static String testPageURL =
            "http://www.compendiumdev.co.uk" +
                    "/selenium/javascript_countdown.html";

    @Before
    public  void startDriver() {
        driver = new FirefoxDriver();
        driver.navigate().refresh();
        driver.navigate().to(testPageURL);
        wait = new WebDriverWait(driver, 100);

        countdown = driver.findElement(By.id("javascript_countdown_time"));
        new WebDriverWait(driver,100).until(
                (ExpectedConditions.visibilityOf(countdown)));
    }

    @After
    public  void closeDriver() {
        driver.quit();
    }

    @Test
    public void waitForElement(){
        String theTime = new FluentWait<WebElement> (countdown).
                withTimeout(10, TimeUnit.SECONDS).
                pollingEvery(100,TimeUnit.MILLISECONDS).
                until(new Function<WebElement, String>(){
                    @Override
                    public String apply(WebElement element) {
                        return element.getText().endsWith("04") ? element.getText() : null;
                    }
                }
                );
        Assert.assertEquals("Expected a different time", "01:01:04", theTime);
    }
}
