package ScreenShots;

import DriverManager.DriverManager;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.HasCapabilities;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;

import java.io.File;
import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.fail;

public class TakingScreenShot {

    @Test
    public void gotPage() throws IOException {
        WebDriver driver = new FirefoxDriver();
        driver.get("http://seleniumsimplified.com");

        TakesScreenshot snapper = (TakesScreenshot)driver;

        File tempScreenshot = snapper.getScreenshotAs(OutputType.FILE);

        System.out.println(tempScreenshot.getAbsolutePath());

        File myScreenShotDir = new File ("C:");

        myScreenShotDir.mkdirs();

        File myScreenshot = new File(myScreenShotDir, "gotoPageScreen.png");
        FileUtils.moveFile(tempScreenshot, myScreenshot);

        Assert.assertTrue(myScreenshot.length() > 0L);

        driver.get("file://" + myScreenshot.getAbsolutePath());
    }

    @Test
    public void capabilitiesTest() {
        WebDriver driver = new FirefoxDriver();
        driver.get("http://seleniumsimplified.com");

        if(((HasCapabilities)driver).getCapabilities().is(CapabilityType.TAKES_SCREENSHOT)) {
            TakesScreenshot snapper = (TakesScreenshot)driver;
            File tempImageFile = snapper.getScreenshotAs(OutputType.FILE);

            Assert.assertTrue(tempImageFile.length() > 0L);

            System.out.println("Temp file written to " + tempImageFile.getAbsolutePath());
            driver.get("File://" + tempImageFile.getAbsolutePath());
        } else {
            fail("Driver did not support screenshots");
        }
    }
}
