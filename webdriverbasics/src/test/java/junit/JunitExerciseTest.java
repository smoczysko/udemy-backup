package junit;

import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

/**
 * Created by robert.kmieciak on 07.12.13.
 */
public class JunitExerciseTest {

    static String homeUrl;
    String whatWeDo;
    static WebDriver driver = new HtmlUnitDriver();

    @BeforeClass
    public static void beforeClass(){
        homeUrl = "http://www.cognifide.com";
    }

    @Before
    public void beforeTest() {
        whatWeDo = "http://cognifide.com/what-we-do";
    }
    @Test
    public void goHomePageTest() {
        driver.get(homeUrl);
        Assert.assertTrue("Page title is wrong", driver.getTitle().equals("Cognifide"));
    }

    @Test
    public void goWhatWeDoPageTest() {
        driver.navigate().to(whatWeDo);
        Assert.assertFalse("We are not on What we do page", driver.getCurrentUrl().contains("Selenium"));
    }

    @AfterClass
    public static void afterClass() {
        driver.quit();
    }
}
