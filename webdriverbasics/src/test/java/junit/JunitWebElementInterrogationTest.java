package junit;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by robert.kmieciak on 08.12.13.
 */
public class JunitWebElementInterrogationTest {
    
    static WebDriver driver;
    
    @BeforeClass
    public static void startDriver(){
        driver = new FirefoxDriver();    
    }
    
    @Test
    public void WebElementInterrogationTest(){
        final String testPageURL =
                "http://www.compendiumdev.co.uk" +
                "/selenium/basic_web_page.html";

        driver.navigate().to(testPageURL);

        WebElement para1 = driver.findElement(By.id("para1"));

        Assert.assertEquals("Wrong text in paragraph 1", para1.getText(), "A paragraph of text");
    }

    @AfterClass
    public static void closeDriver(){
        driver.quit();
    }
}
