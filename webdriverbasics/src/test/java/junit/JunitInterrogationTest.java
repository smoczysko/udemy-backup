package junit;

import junit.framework.Assert;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by robert.kmieciak on 08.12.13.
 */
public class JunitInterrogationTest {

    static WebDriver driver;

    @BeforeClass
    public static void startDriver() {
        driver = new FirefoxDriver();
    }

    @Test
    public void interrogationTest() {
        driver.get("http://www.compendiumdev.co.uk/selenium/basic_web_page.html");
        assertThat(driver.getTitle(), is("Basic Web Page Title"));
        //Assert.assertTrue("You're on wrong page", driver.getTitle().equals("Basic Web Page Title"));

        Assert.assertTrue("You're under wrong URL", driver.getCurrentUrl().equals("http://www.compendiumdev.co.uk/selenium/basic_web_page.html"));

        String pageSource = driver.getPageSource();
        Assert.assertTrue("Page source does not contain provided string", pageSource.contains("A paragraph of text"));
        System.out.println(pageSource);

        driver.quit();
    }

    @AfterClass
    public static void closeDriver(){
        driver.quit();
    }
}
