package junit;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by robert.kmieciak on 08.12.13.
 */
public class JunitFindByTest {

    static WebDriver driver;
    final static String testPageURL =
            "http://www.compendiumdev.co.uk" +
                    "/selenium/find_by_playground.php";

    @BeforeClass
    public static void startDriver(){
        driver = new FirefoxDriver();
        driver.navigate().to(testPageURL);
    }

    @Test
    public void WebElementFindByIdTest(){
        WebElement find = driver.findElement(By.id("p1"));

        Assert.assertEquals("Wrong text in paragraph", "This is a paragraph text", find.getText());
    }

    @Test
    public void WebElementFindByLinkTextTest(){
        WebElement find = driver.findElement(By.linkText("jump to para 13"));

        Assert.assertEquals("a39", find.getAttribute("id"));
    }

    @Test
    public void WebElementFindByPartialLinkTextTest(){
        WebElement find = driver.findElement(By.partialLinkText("to para 10"));

        Assert.assertEquals("Wrong text in paragraph ", "jump to para 10", find.getText());
    }

    @Test
    public void WebElementFindByNameTest(){
        WebElement find = driver.findElement(By.name("pName30"));

        Assert.assertEquals("Wrong text in paragraph ", "nested para text", find.getText());
    }

    @Test
    public void WebElementFindByClassNameTest(){
        WebElement find = driver.findElement(By.className("normal"));

        Assert.assertEquals("Wrong text in paragraph ", "This is a paragraph text", find.getText());
    }

    @AfterClass
    public static void closeDriver(){
        driver.quit();
    }
}
