package junit;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by robert.kmieciak on 08.12.13.
 */
public class FindElementsTest {

    static WebDriver driver;
    final static String testPageURL =
            "http://www.compendiumdev.co.uk" +
                    "/selenium/find_by_playground.php";

    @BeforeClass
    public static void startDriver() {
        driver = new FirefoxDriver();
        driver.navigate().to(testPageURL);
    }

    @Test
    public void findElementsDiv(){
        List<WebElement> elements;
        elements = driver.findElements(By.tagName("div"));

        Assert.assertEquals(19, elements.size());
    }

    @Test
    public void findElementsLinks(){
        List<WebElement> elements;
        elements = driver.findElements(By.partialLinkText("jump to para"));

        Assert.assertEquals(25, elements.size());
    }

    @Test
    public void findElementsBonus(){
        List<WebElement> elements;
        elements = driver.findElements(By.tagName("p"));

        int nestedCount = 0;
        for(WebElement e : elements) {
            if(e.getText().contains("nested para")){
                nestedCount++;
            }
        }
        Assert.assertEquals(16, nestedCount);
        Assert.assertEquals(41, elements.size());
    }

    @AfterClass
    public static void closeDriver() {
        driver.quit();
    }
}
