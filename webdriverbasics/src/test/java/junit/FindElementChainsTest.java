package junit;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ByIdOrName;
import org.openqa.selenium.support.pagefactory.ByChained;

import java.util.List;

/**
 * Created by robert.kmieciak on 08.12.13.
 */
public class FindElementChainsTest {

    static WebDriver driver;
    final static String testPageURL =
            "http://www.compendiumdev.co.uk" +
                    "/selenium/find_by_playground.php";

    @BeforeClass
    public static void startDriver() {
        driver = new FirefoxDriver();
        driver.navigate().to(testPageURL);
    }

    @Test
    public void findElementsChained(){
        WebElement element = driver.findElement(By.id("div1")).
                findElement(By.name("pName3")).
                findElement(By.tagName("a"));

        Assert.assertEquals("expected other id", "a3", element.getAttribute("id"));
    }

    @Test
    public void findElementsByChained(){
        WebElement element = driver.findElement(
                new ByChained(By.id("div1"),
                        By.name("pName3"),
                        By.tagName("a")));

        Assert.assertEquals("expected other id", "a3", element.getAttribute("id"));
    }

    @Test
    public void findElementsByIdOrName(){
        WebElement element = driver.findElement(
                new ByIdOrName("pName2"));

        Assert.assertEquals("expected other name", "pName2", element.getAttribute("name"));
    }

    @AfterClass
    public static void closeDriver() {
        driver.quit();
    }
}
