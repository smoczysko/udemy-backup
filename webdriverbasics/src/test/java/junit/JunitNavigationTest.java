package junit;

import junit.framework.Assert;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.Date;

/**
 * Created by robert.kmieciak on 07.12.13.
 */
public class JunitNavigationTest {

    static WebDriver driver = new FirefoxDriver();
    String selenium = "/selenium",
    search = "/selenium/search.php",
    htmlForm = "/selenium/basic_html_form.html",
    webPageTitle = "/selenium/basic_web_page.html",
    refresh = "/selenium/refresh.php";

    @BeforeClass
    public static void beforeClass() {
        driver.get("http://compendiumdev.co.uk");
    }

    @Test
    public void navigateToFirst() {

        driver.navigate().to("http://compendiumdev.co.uk" + selenium);
        Assert.assertTrue("Wrong title", driver.getTitle().contains("Selenium Simplified"));
        driver.navigate().back();
        Assert.assertTrue("Navigate back failed", driver.getTitle().contains("Software Testing Essays, Book Reviews and Information"));
    }

    @Test
    public void navigateForward() {

        driver.navigate().forward();
        Assert.assertTrue("Wrong title", driver.getTitle().contains("Selenium Simplified"));
    }

    @Test
    public void navigateToSearch() {

        driver.navigate().to("http://compendiumdev.co.uk" + search);
        Assert.assertEquals("Navigate failed", "Selenium Simplified Search Engine", driver.getTitle());
    }

    @Test
    public void navigateToBasicForms() {

        driver.navigate().to("http://compendiumdev.co.uk" + htmlForm);
        Assert.assertEquals("Navigate failed", "HTML Form Elements", driver.getTitle());
    }

    @Test
    public void navigateToWebPage() {

        driver.navigate().to("http://compendiumdev.co.uk" + webPageTitle);
        Assert.assertEquals("Navigate failed", "Basic Web Page Title", driver.getTitle());
    }

    @Test
    public void navigateToRefresh() {
        driver.navigate().to("http://compendiumdev.co.uk" + refresh);
        String title = driver.getTitle();
        driver.navigate().refresh();
        Assert.assertFalse(title.equals(driver.getTitle()));
    }

    @AfterClass
    public static void afterClass() {
        driver.quit();
    }
}
