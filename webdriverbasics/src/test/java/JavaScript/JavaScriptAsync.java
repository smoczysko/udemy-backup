package JavaScript;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static org.junit.internal.matchers.StringContains.containsString;

/**
 * Created by robert.kmieciak on 02.01.14.
 */
public class JavaScriptAsync {
    private WebDriver driver;
    private WebDriverWait wait;
    int actionsCount;
    final static String testPageURL =
            "http://www.compendiumdev.co.uk" +
                    "/selenium/basic_ajax.html";
    private WebElement queryInput;
    private WebElement submitButton;

    @Before
    public  void startDriver() {
        driver = new FirefoxDriver();
        driver.navigate().to(testPageURL);
        wait = new WebDriverWait(driver, 100);
        driver.manage().deleteAllCookies();
    }

    @After
    public  void closeDriver() {
        driver.quit();
    }

    @Test
    public void javaScriptAsyncExampleTest() {
        driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);

        long start = System.currentTimeMillis();
        ((JavascriptExecutor) driver).executeAsyncScript(
                "window.setTimeout(arguments[arguments.length - 1], 500);");

        System.out.println(
                "Elapsed time: " + (System.currentTimeMillis() - start));

        Assert.assertTrue("Elapsed time should be greater than 500 milis",
                (System.currentTimeMillis() - start) > 500);
    }

    @Test
    public void useXMLHttpRequest() {
        driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);

        Object response = ((JavascriptExecutor) driver).executeAsyncScript(
                "var callback = arguments[arguments.length - 1];" +
                        "var xhr = new XMLHttpRequest();" +
                        "xhr.open('GET', '/selenium/ajaxselect.php?id=2', true);" +
                        "xhr.onreadystatechange = function() {" +
                        " if(xhr.readyState == 4) {" +
                        "callback(xhr.responseText);" +
                        "}" +
                        "};" +
                        "xhr.send();");

        System.out.println((String)response);

        Assert.assertThat((String) response, containsString("{optionValue:10, optionDisplay: 'C++'}"));
    }
}
