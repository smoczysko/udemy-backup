package JavaScript;

import com.gargoylesoftware.htmlunit.javascript.background.JavaScriptExecutor;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.junit.Assert;

/**
 * Created by robert.kmieciak on 18.12.13.
 */
public class JavaScriptIntroduction {
    private WebDriver driver;
    private WebDriverWait wait;
    int actionsCount;
    final static String testPageURL =
            "http://www.compendiumdev.co.uk" +
                    "/selenium/canvas_basic.html";
    private WebElement queryInput;
    private WebElement submitButton;

    @Before
    public  void startDriver() {
        driver = new FirefoxDriver();
        driver.navigate().to(testPageURL);
        wait = new WebDriverWait(driver, 100);
        driver.manage().deleteAllCookies();
    }

    @After
    public  void closeDriver() {
        driver.quit();
    }

    @Test
    public void javaScriptExampleTest() {
        JavascriptExecutor js = (JavascriptExecutor)driver;

        checkActionsCount(2);

        js.executeScript("draw(1, 150,150,40, '#FF1C0A'); ");

        checkActionsCount(3);
    }

    @Test
    public void javScriptFirstExercise() {
        JavascriptExecutor js = (JavascriptExecutor)driver;

        checkActionsCount(2);

        js.executeScript("clearCanvas()");

        checkActionsCount(3);

        for(int testLoop=0; testLoop<10; testLoop++){
            js.executeScript("draw(0, arguments[0], arguments[1], 20, arguments[2]);",
                    testLoop*20,
                    testLoop*20,
                    "#" + testLoop + testLoop + "0000");
        }
        checkActionsCount(13);
    }

    @Test
    public void javScriptSecondExercise() {
        JavascriptExecutor js = (JavascriptExecutor)driver;

        Assert.assertEquals(70L,
                js.executeScript(
                        "return (arguments[0] + arguments[1]);",
                        50,
                        20));
    }

    @Test
    public void changeTitle() {
        JavascriptExecutor js = (JavascriptExecutor)driver;

        js.executeScript("document.title=arguments[0]", "This is new title");

        Assert.assertEquals("Title is wrong", "This is new title", driver.getTitle());
    }

    public void checkActionsCount(int expectedCount){
        actionsCount = driver.findElements(By.cssSelector("#commandlist li")).size();

        Assert.assertEquals(expectedCount, actionsCount);
    }
}
