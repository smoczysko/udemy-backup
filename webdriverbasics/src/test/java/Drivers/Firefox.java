package Drivers;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import static org.hamcrest.core.Is.is;

/**
 * Created by robert.kmieciak on 03.01.14.
 */
public class Firefox {

    @Test
    public void firefoxDriverTests() {
        WebDriver firefox = new FirefoxDriver();

        firefox.get("http://www.compendiumdev.co.uk/selenium/basic_html_form.html");

        Assert.assertThat(firefox.getTitle(), is("HTML Form Elements"));

        firefox.quit();
    }

    @Test
    public void firefoxDriverWithProfile() {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setEnableNativeEvents(true);

        WebDriver firefox = new FirefoxDriver(profile);

        firefox.get("http://www.compendiumdev.co.uk/selenium/basic_html_form.html");

        Assert.assertThat(firefox.getTitle(), is("HTML Form Elements"));

        firefox.quit();
    }

//    @Test
//    public void firefoxDriverWithCapabilitiesForProxy(){
//
//        org.openqa.selenium.Proxy proxy = new Proxy();
//        proxy.setHttpProxy(Driver.PROXY)
//                .setFtpProxy(Driver.PROXY)
//                .setSslProxy(Driver.PROXY);
//
//        DesiredCapabilities capabilities = new DesiredCapabilities();
//        capabilities.setCapability(CapabilityType.PROXY, proxy);
//
//        WebDriver firefox = new FirefoxDriver(capabilities);
//
//        firefox.get("http://www.compendiumdev.co.uk/selenium/basic_html_form.html");
//
//        Assert.assertThat(firefox.getTitle(), is("HTML Form Elements"));
//
//        firefox.quit();
//    }
}
