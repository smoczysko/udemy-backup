package Drivers;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import static org.hamcrest.core.Is.is;

/**
 * Created by robert.kmieciak on 03.01.14.
 */
public class HtmlUnit {

    @Test
    public void basicHtmlUnitTest(){
        WebDriver htmlUnit = new HtmlUnitDriver(BrowserVersion.FIREFOX_17);

        htmlUnit.get("http://www.compendiumdev.co.uk/selenium/basic_html_form.html");

        Assert.assertThat(htmlUnit.getTitle(), is("HTML Form Elements"));

        htmlUnit.quit();
    }

    @Test
    public void basicHtmlUnitWithJavaScriptTest(){
        WebDriver htmlUnit = new HtmlUnitDriver(true);

        htmlUnit.get("http://www.compendiumdev.co.uk/selenium/basic_html_form.html");

        Assert.assertThat(htmlUnit.getTitle(), is("HTML Form Elements"));

        htmlUnit.quit();
    }
}
