package Cookies;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by robert.kmieciak on 17.12.13.
 */
public class CookiesIntroduction {
    private WebDriver driver;
    private WebDriverWait wait;
    final static String testPageURL =
            "http://www.compendiumdev.co.uk" +
                    "/selenium/search.php";
    private WebElement queryInput;
    private WebElement submitButton;

    @Before
    public  void startDriver() {
        driver = new FirefoxDriver();
        driver.navigate().to(testPageURL);
        wait = new WebDriverWait(driver, 100);
        driver.manage().deleteAllCookies();
        refreshPageObjects();
    }

    @After
    public  void closeDriver() {
        driver.quit();
    }

    @Test
    public void cookiesIntroductionTest() {
        driver.manage().deleteAllCookies();
        driver.navigate().refresh();

        Cookie aCookie = driver.manage().getCookieNamed("SeleniumSimplifiedLastSearch");

        Cookie.Builder anotherNewCookie = new Cookie.Builder("myCookie", "myValue");

        anotherNewCookie.isSecure(true);

        anotherNewCookie.build();

        Assert.assertEquals("Should be no last search cookie", null, aCookie);
    }

    @Test
    public void numberOfVisits() {
        driver.manage().deleteAllCookies();
        driver.navigate().refresh();

        Cookie numberOfVisitsCookie = driver.manage().getCookieNamed("seleniumSimplifiedSearchNumVisits");

        Assert.assertEquals("You were searching more than once", "1", numberOfVisitsCookie.getValue());
    }

    @Test
    public void hackVisitCookie() {
        queryInput.clear();
        queryInput.sendKeys("Cookie Test");
        submitButton.click();

        refreshPageObjects();

        Cookie hackCookie = driver.manage().getCookieNamed("seleniumSimplifiedSearchNumVisits");

        Assert.assertEquals("You were searching more than once", "1", hackCookie.getValue());

        Cookie aNewCookie = new Cookie( hackCookie.getName(),
                String.valueOf(42),
                hackCookie.getDomain(),
                hackCookie.getPath(),
                hackCookie.getExpiry(),
                hackCookie.isSecure());

        driver.manage().deleteCookie(hackCookie);
        driver.manage().addCookie(aNewCookie);

        queryInput.clear();
        queryInput.sendKeys("Cookie Test");
        submitButton.click();

        hackCookie = driver.manage().getCookieNamed("seleniumSimplifiedSearchNumVisits");
        Assert.assertEquals("43", hackCookie.getValue());
    }

    private void refreshPageObjects(){
        queryInput = driver.findElement(By.name("q"));
        submitButton = driver.findElement(By.name("btnG"));
    }
}
