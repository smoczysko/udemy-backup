package CSSXpath;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by robert.kmieciak on 08.12.13.
 */
public class XpathTest {

    static WebDriver driver;
    final static String testPageURL =
            "http://www.compendiumdev.co.uk" +
                    "/selenium/find_by_playground.php";

    @BeforeClass
    public static void startDriver() {
        driver = new FirefoxDriver();
        driver.navigate().to(testPageURL);
    }

    @Test
    public void findElementById(){
//        WebElement element = driver.findElement(By.id("p31"));

//        WebElement element = driver.findElement(By.cssSelector("#p31"));

        WebElement element = driver.findElement(By.xpath("//p[@id='p31']"));

        Assert.assertEquals("pName31", element.getAttribute("name"));
    }

    @Test
    public void findElementByName(){
//        WebElement element = driver.findElement(By.name("ulName1"));

//        WebElement element = driver.findElement(By.cssSelector("[name='ulName1']"));

        WebElement element = driver.findElement(By.xpath("//ul[@name='ulName1']"));

        Assert.assertEquals("ul1", element.getAttribute("id"));
    }

    @Test
    public void findElementByClassName(){
//        WebElement element = driver.findElement(By.className("specialDiv"));

//        WebElement element = driver.findElement(By.cssSelector(".specialDiv"));
//        WebElement element = driver.findElement(By.cssSelector("div[class='specialDiv']"));

        WebElement element = driver.findElement(By.xpath("//div[@class='specialDiv']"));

        Assert.assertEquals("div1", element.getAttribute("id"));
    }

    @Test
    public void findElementByTagName(){
//        WebElement element = driver.findElement(By.tagName("li"));

//        WebElement element = driver.findElement(By.cssSelector("li"));

        WebElement element = driver.findElement(By.xpath("//li"));

        Assert.assertEquals("liName1", element.getAttribute("name"));
    }

    @AfterClass
    public static void closeDriver() {
        driver.quit();
    }
}
