package CSSXpath;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import static org.hamcrest.core.Is.is;

/**
 * Created by robert.kmieciak on 08.12.13.
 */
public class GetTitleReplacementTest {

    static WebDriver driver;
    final static String testPageURL =
            "http://www.compendiumdev.co.uk" +
                    "/selenium/find_by_playground.php";
    String pageTitle = "Welcome to the Find By Playground";

    @BeforeClass
    public static void startDriver() {
        driver = new FirefoxDriver();
        driver.navigate().to(testPageURL);
    }

    @Test
    public void getTitleWithoutGetTitle(){
        WebElement element = driver.findElement(By.xpath("//title"));

        Assert.assertThat(pageTitle, is(element.getAttribute("text")));
      //  Assert.assertEquals(pageTitle, element.getText());
    }

    @AfterClass
    public static void closeDriver() {
        driver.quit();
    }
}
