package com.seleniumsimplified.webdriver;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import static junit.framework.Assert.assertTrue;

public class FirstTest
{
    @Test
    public void driverIsTheKing()
    {
        WebDriver driver = new HtmlUnitDriver();

        driver.get("http://www.cognifide.com");
        assertTrue(driver.getTitle().startsWith("Cognifide"));

        driver.navigate().to("http://www.cognifide.com/what-we-do");
        assertTrue(driver.getCurrentUrl().equals("http://www.cognifide.com/what-we-do"));

        driver.navigate().back();
        assertTrue(driver.getTitle().startsWith("Cognifide"));
    }

    @Test
    public void firefoxIsSupportedByWebDriver()
    {
        WebDriver driver = new FirefoxDriver();

        driver.get("http://www.cognifide.com");
        assertTrue(driver.getTitle().startsWith("Cognifide"));

        driver.navigate().to("http://www.cognifide.com/what-we-do");
        assertTrue(driver.getCurrentUrl().equals("http://www.cognifide.com/what-we-do"));

        driver.navigate().back();
        assertTrue(driver.getTitle().startsWith("Cognifide"));

        driver.quit();
    }
}
